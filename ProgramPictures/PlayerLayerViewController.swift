
/***
 **Module Name:  PlayerLayerView Controller.
 **File Name :  PlayerLayerView Controller.swift
 **Project :   theprogrampictures.com
 **Copyright(c) : Program Pictures.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Player.
 */

import UIKit
import AVKit

protocol playerdelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
       func callwatchlist(dict:NSDictionary)
}

class PlayerLayerViewController: UIViewController,playerEndDelegate {
    
    var playerLayer:AVPlayerLayer!
   
   
    var playerItem:AVPlayerItem!
    
    var videoUrl,userID,videoID,mainVideoID,deviceID : String!
    var seektime = Float64()
    var updatetime = Float64()
    var isResume = Bool()
    var detdelegate:playerdelegate?
    var resumeTime = Float64()
    var getnextAsset = Bool()
    var UpdateTimer = Timer()
    var isplayEnd = Bool()
    var nextAsset = NSDictionary()
    var isMyList = Bool()
    var nexttimer = Timer()
    var player:AVPlayer!
    var isMenuPressed = Bool()
    var timeObserver:Any?
    var notifyObserver:Any?
    var isdurPlay = Bool()
    var avplayerController = AVPlayerViewController()
    var issearch = Bool()
    var currentDuration = 0
    var previousDuration = 0
    var notPlayCheckCnt = 0
    var duration = 0
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        avplayerController.view.frame = self.view.frame
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isdurPlay = false
        self.getnextAsset = false
        playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        player = AVPlayer(playerItem:playerItem)
        player.replaceCurrentItem(with: playerItem)
        avplayerController.player = player
        self.view.addSubview(avplayerController.view)
        self.addChildViewController(avplayerController)
        self.isMenuPressed = false
        playVideo(userId: self.userID, videoId: self.videoID, deviceId: self.deviceID, MyList: self.isMyList)
    
    }
    
    func handleMenuPress()
    {
        self.isMenuPressed = true
        UpdateTimer.invalidate()
        player.pause()
        player.removeTimeObserver(timeObserver as Any)
        player = nil
        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
     
        if !issearch
        {
//            if isfromplayend
//            {
//                for viewcontroller in (self.navigationController?.viewControllers)!
//                {
//                    if viewcontroller.isKind(of: BBFDetailPageViewController.self)
//                    {
//                        let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
//                    }
//                }
//            }
//            else
//            {
            
                let _ = self.navigationController?.popViewController(animated: true)
           // }
        }
        else
        {
            dismiss(animated: true, completion: nil)
        }
       
    }
    
    // playvideo Implementation
    func playVideo(userId:String,videoId:String,deviceId:String,MyList:Bool) {
        var updateSeekCnt = 0
        userID = userId
        videoID = videoId
        deviceID = deviceId
        isMyList = MyList
        if isResume
        {
           
            let targetTime = CMTime(seconds: resumeTime, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player.play()
        }
        else
        {
            player.play()
        }
      
        timeObserver = (player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                
                if self.player.currentItem?.status == .failed
                {
                    self.playerItem = nil
                    if !self.issearch
                    {
                        let alertview = UIAlertController(title: "Video Error", message: "Unable to Play video" , preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                            UIAlertAction in
                            let _ = self.navigationController?.popViewController(animated: true)
                        })
                        alertview.addAction(defaultAction)
                        self.navigationController?.present(alertview, animated: true, completion: nil)
                        //   let _ = self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else if self.player?.currentItem?.status == .readyToPlay
                {
                    self.duration = Int(CMTimeGetSeconds((self.playerItem.duration)))
                   
                    //   }
                    if(updateSeekCnt >= 10)
                    {
                        updateSeekCnt = 0
                        self.UpdateseekTime()
                        
                    }
                    updateSeekCnt = updateSeekCnt + 1
                    
                    self.seektime = CMTimeGetSeconds((self.playerItem.currentTime()))
                    self.currentDuration = Int ( CMTimeGetSeconds((self.playerItem.currentTime())))
                    
                    
                    
                    if(self.currentDuration != self.previousDuration)
                    {
                        self.previousDuration =  self.currentDuration
                        self.notPlayCheckCnt = 0
                    }
                    else
                    {
                        self.notPlayCheckCnt = self.notPlayCheckCnt + 1
                    }
                    if ( self.notPlayCheckCnt >= 10 )
                    {
                        self.playerEnd()
                    }
                    if ((self.duration - self.currentDuration) <= 30) && (self.getnextAsset == false)
                    {
                        self.getnextAsset = true
                 
                        if self.issearch == false
                        {
                            self.nextAssetData()
                            self.isMenuPressed = true
                        }
                    }
                    
                    if  (self.currentDuration + 1) >= Int(self.duration) && self.isdurPlay == false
                    {
                        self.isdurPlay = true
                        if self.issearch == false
                        {
                            self.playerEnd()
                        }
                    }
                    
                    
                    /*   if (stime >= (dur-2)) && (self.isdurPlay == false)
                     {
                     self.isdurPlay = true
                     if self.issearch == false
                     {
                     if self.isplayendcalled == false
                     {
                     self.isplayendcalled = true
                     self.playerEnd()
                     }
                     }
                     }*/
                }
        }))
        
 
    
     //   UpdateTimer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.UpdateseekTime), userInfo: nil, repeats: true)
       
    }
    
    func playnextVideo()
    {
        let playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        player = AVPlayer(playerItem: playerItem)
        player?.play()
    }
    
    // seektime
    func UpdateseekTime()
    {
        let parameters = ["updateSeekTime":["userId": userID as AnyObject, "videoId": (videoID) as AnyObject, "seekTime": self.seektime,"carouselName":"Program Pictures" as AnyObject]]
       
        ApiManager.sharedManager.postDataWithJson(url: kUpdateseekUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["watchedVideo"] != nil
                {
                    self.updatetime = Float64((((JSON)["watchedVideo"] as! NSDictionary)["seekTime"]) as! Float64)
                    UserDefaults.standard.set(self.updatetime, forKey: "seektime")
                    UserDefaults.standard.synchronize()
//                    if self.updatetime > 0
//                    {
//                        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
//                    }
                }
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                  let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    // Next Asset Data
    func nextAssetData()
    {
  
      //  getnextAsset = true
        let parameters = ["getNextPlay": ["videoId": videoID, "userId": self.userID,"myList":self.isMyList,"deviceId":self.deviceID]]
        ApiManager.sharedManager.postDataWithJson(url:kNextAssetUrl ,parameters: parameters as [String : [String : AnyObject]])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                self.nextAsset = responseDict as! NSDictionary
            }
            else
            {
                
            }
        }
        
    }
    // player end
    func playerEnd()
    {
        print("Iam in player end")
        seektime = 0.0
        self.isplayEnd = true
        UpdateseekTime()
        UpdateTimer.invalidate()
    
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let playerEnd = storyBoard.instantiateViewController(withIdentifier: "playerEnd") as! PlayerEndViewController
        playerEnd.userID = userID
        playerEnd.deviceID = deviceID
        if self.nextAsset.count != 0
        {
            playerEnd.getData(getnextData: self.nextAsset)
            playerEnd.playEnddelegate = self
            //    isdurPlay = false
            self.navigationController?.pushViewController(playerEnd, animated: true)

        }
        else
        {
          self.dismiss(animated: true, completion: nil)
        }

             
    }
        func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,videoURL:String)
        {
            self.videoID = ""
    
            self.userID = userId
            self.videoID = videoId
      
            self.deviceID = deviceId
            self.isMyList = MyList
            self.videoUrl = videoURL
        }
    func addrecentwatchList(dict:NSDictionary)
    {
      
        self.detdelegate?.callwatchlist(dict: dict)
        
    }
}


